# Falkon

Falkon is a lightweight cross-platform web browser formerly called QupZilla.
Project Falkon has now been absorbed into the KDE family, based on QtWebEngine rendering Engine.


## REQUIRED DEPENDENCIES ##
QT5  
QTWebEngine  
extra-cmake-modules  
ki18n (Alienbob)  
kio (Alienbob)  

## IF YOU HAD AN EARLIER VERSION OF QUPZILLA ##
remove or rename the old qtwebkit profile, as it will conflict.
~/.config/qupzilla
