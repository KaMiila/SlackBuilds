# Winetricks

Winetricks is an easy way to work around problems in Wine.  

It has a menu of supported games/apps for which it can do all the workarounds automatically. It also allows the installation of missing DLLs and tweaking of various Wine settings.

## Installing

**This pulls from git**  

This script will autmatically pull the latest winetricks script from github and create an installable slackware package for it so you can track winetricks in pkgtools.

## Updating

Two options available to you.  
* If you want pkgtools to track updates, re-run this script when winetricks informs you there is an update.
* Or (as root) issue this command without the quotes "winetricks --self-update"  

The problem with the second option, pkgtools **may** be unable to fully remove the script during a removepkg command due to changes made independently.  

## Dependencies

cabextract  
wine (or wine-staging)  

### Homepage

<https://github.com/Winetricks/winetricks>
