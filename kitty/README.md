# Kitty

**THIS PULLS FROM GIT**

[**Kitty**][Kitty] The fast, fearful, GPU, terminal emulator

## Features

 * Offloads rendering to the GPU for lower system load and buttery
   smooth scrolling.  Uses threaded rendering to minimize input latency.
 * Supports all modern terminal features: graphics (images), unicode,
   true-color, OpenType ligatures, mouse protocol, focus tracking, bracketed
   paste and several new terminal protocol extensions.
 * Supports tiling multiple terminal windows side by side in different
   layouts without needing to use an extra program like tmux.
 * Can be controlled from scripts or the shell prompt, even over SSH.
 * Has a framework for Kittens, small terminal programs that can be used
   to extend kitty's functionality. For example, they are used for Unicode
   input, Hints and Side-by-side diff.
 * Supports startup sessions which allow you to specify the window/tab layout,
   working directories and programs to run on startup.
 * Cross-platform: kitty works on Linux and macOS, but because it uses only
   OpenGL for rendering, it should be trivial to port to other Unix-like platforms.
 * Allows you to open the scrollback buffer in a separate window using arbitrary
   programs of your choice. This is useful for browsing the history comfortably
   in a pager or editor.
 * Has multiple copy/paste buffers, like vim.

## Dependencies
Sphinx <-- ensure it is a captial S and not lower case.

[Kitty]: https://github.com/kovidgoyal/kitty
